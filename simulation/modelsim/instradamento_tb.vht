-- Copyright (C) 1991-2013 Altera Corporation
-- Your use of Altera Corporation's design tools, logic functions 
-- and other software and tools, and its AMPP partner logic 
-- functions, and any output files from any of the foregoing 
-- (including device programming or simulation files), and any 
-- associated documentation or information are expressly subject 
-- to the terms and conditions of the Altera Program License 
-- Subscription Agreement, Altera MegaCore Function License 
-- Agreement, or other applicable license agreement, including, 
-- without limitation, that your use is for the sole purpose of 
-- programming logic devices manufactured by Altera and sold by 
-- Altera or its authorized distributors.  Please refer to the 
-- applicable agreement for further details.

-- ***************************************************************************
-- This file contains a Vhdl test bench template that is freely editable to   
-- suit user's needs .Comments are provided in each section to help the user  
-- fill out necessary details.                                                
-- ***************************************************************************
-- Generated on "11/23/2018 14:45:18"
                                                            
-- Vhdl Test Bench template for design  :  instradamento
-- 
-- Simulation tool : ModelSim-Altera (VHDL)
-- 

LIBRARY ieee;                                               
USE ieee.std_logic_1164.all;                                

ENTITY instradamento_tb_vhd_tst IS
END instradamento_tb_vhd_tst;
ARCHITECTURE instradamento_arch OF instradamento_tb_vhd_tst IS
-- constants                                                 
-- signals                                                   
SIGNAL CLOCK_50 : STD_LOGIC := '1';
SIGNAL HEX0 : STD_LOGIC_VECTOR(6 DOWNTO 0) := "1111111";
SIGNAL HEX1 : STD_LOGIC_VECTOR(6 DOWNTO 0) := "1111111";
SIGNAL SW : STD_LOGIC_VECTOR(0 DOWNTO 0) := "0";
SIGNAL period : TIME := 20 ns;
COMPONENT instradamento
	PORT (
	CLOCK_50 : IN STD_LOGIC;
	HEX0 : OUT STD_LOGIC_VECTOR(6 DOWNTO 0);
	HEX1 : OUT STD_LOGIC_VECTOR(6 DOWNTO 0);
	SW : IN STD_LOGIC_VECTOR(0 DOWNTO 0)
	);
END COMPONENT;
BEGIN
	i1 : instradamento
	PORT MAP (
-- list connections between master ports and signals
	CLOCK_50 => CLOCK_50,
	HEX0 => HEX0,
	HEX1 => HEX1,
	SW => SW
	);
clock : PROCESS                                                                                    
BEGIN

CLOCK_50 <= '1';
WAIT FOR period/2;
CLOCK_50 <= '0';                                            
WAIT FOR period/2;
                                                       
END PROCESS clock;                                           
always : PROCESS                                              
                                      
BEGIN                                                         
SW(0) <= '0', '1' AFTER 11*period, '0' AFTER 12*period;
WAIT FOR 15*period;
                                                        
END PROCESS always;                                          
END instradamento_arch;
