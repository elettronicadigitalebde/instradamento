onerror {quit -f}
vlib work
vlog -work work instradamento.vo
vlog -work work instradamento.vt
vsim -novopt -c -t 1ps -L cycloneii_ver -L altera_ver -L altera_mf_ver -L 220model_ver -L sgate work.instradamento_vlg_vec_tst
vcd file -direction instradamento.msim.vcd
vcd add -internal instradamento_vlg_vec_tst/*
vcd add -internal instradamento_vlg_vec_tst/i1/*
add wave /*
run -all
