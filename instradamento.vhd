LIBRARY ieee;
USE ieee.std_logic_1164.all;

ENTITY instradamento IS 
	PORT (CLOCK_50 :in std_logic;
			SW			:in std_logic_vector(0 DOWNTO 0);
			HEX0		:out std_logic_vector(6 DOWNTO 0);
			HEX1     :out std_logic_vector(6 DOWNTO 0));
END;

ARCHITECTURE arch_instradamento OF instradamento IS
	
	COMPONENT main IS
		PORT(clk    :in std_logic;
			  serial :in std_logic;
			  reset  :in std_logic;
			  cifra0 :out std_logic_vector(6 DOWNTO 0);
			  cifra1 :out std_logic_vector(6 DOWNTO 0));
	END COMPONENT;
	
	SIGNAL temp0,temp1 :std_logic_vector(6 DOWNTO 0);
	SIGNAL tempSerial :std_logic := '0';

	BEGIN
	
		
		PROCESS(CLOCK_50)
			
			VARIABLE cont :integer := 0;
			VARIABLE flag :std_logic := '0';
			VARIABLE tempRand :std_logic_vector(3 DOWNTO 0);
			VARIABLE feedBack :std_logic := '0';

		
			BEGIN
			
				feedBack := NOT(tempRand(3) XOR tempRand(2));

				IF rising_edge(CLOCK_50) THEN
				
					IF (cont = 8) THEN
						tempSerial <= '1';
						cont := 0;
						tempRand := (OTHERS => '0');
						flag := '0';
					ELSE
						tempRand := tempRand(2 DOWNTO 0) & feedBack;
						IF tempRand(3) = '0' AND cont = 0 THEN 
							flag := '1';
						END IF;
						tempSerial <= tempRand(3);	
					END IF;
					
					IF flag = '1' THEN 
					cont := cont + 1;
					END IF;
					
				END IF;
			
			END PROCESS;
			
			y1: main PORT MAP (CLOCK_50, tempSerial, SW(0),temp0,temp1);
			
			
			HEX0 <= temp0;
			HEX1 <= temp1;
		
	
	
	
	END;